import { Routes, RouterModule } from "@angular/router";
import { LayoutComponent } from "./layout.component";

const LAYOUT_ROUTES: Routes = [
	{
		path: "", 
		redirectTo: "Login", 
		pathMatch: "full" 
	},
	{
		path:"Login",
		loadChildren: "../pages/login/login.module#LoginModule"
	},
	{
		path: "Login/:key",
		loadChildren: "../pages/loginAutomatico/loginAutomatico.module#LoginAutomaticoModule"
	},
	{
		path: "Cadastro",
		loadChildren: "../pages/cadastro/cadastro.module#CadastroModule"
	},
	{
		path: "Home",
		component: LayoutComponent,
		children: [
			{
				path: "",
				redirectTo: "Dashboard",
				pathMatch: "full"
			},
			{
				path: "Dashboard",
				loadChildren: "../pages/dashboards/dashboards.module#DashboardsModule"
			},
			{
				path: "MinhaConta",
				children: [
					{
						path: "MeusDados",
						loadChildren: "../pages/meusdados/meusdados.module#MeusDadosModule"
					},
					{
						path: "AlterarSenha",
						loadChildren: "../pages/alterarSenha/alterarsenha.module#AlterarSenhaModule"
					}
				]
			},
			{
				path: "FAQ",
				loadChildren: "../pages/faq/faq.module#FaqModule"
			},
			{
				path: "IndiqueGanhe",
				loadChildren: "../pages/indiqueganhe/indiqueganhe.module#IndiqueGanheModule"
			},
			{
				path: "Programacao",
				loadChildren: "../pages/programacao/programacao.module#ProgramacaoModule"
			},
			{
				path: "Extrato",
				loadChildren: "../pages/extrato/extrato.module#ExtratoModule"
			},
			{
				path: "MeusIndicados",
				loadChildren: "../pages/meusindicados/meusindicados.module#MeusIndicadosModule"
			}
		]
	},

	// 404 Page Not Found
	{ path: "**", redirectTo: "" }
];

export const LayoutRoutes = RouterModule.forChild(LAYOUT_ROUTES);
