import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { } from 'rxjs/add/operator/toPromise';

@Injectable()
export class GlobalState {

  private _data = new Subject<Object>();
  private _dataStream = this._data.asObservable();
  private _subscriptions: Map<string, Array<Function>> = new Map<string, Array<Function>>();

  /* Dados Usuário */
  public username: string = '';
  public male: boolean = true;
  public name: string = "";
  public email: string = "";
  public pontos: number = 0;
  public cpf: string = "";
  public cep: string = "";
  public dtNasc: string = "";
  public indiqueGanheUrl: string;
  public tel: string = "";
  public redeem_pass: string = "";

  /* ###### */

  public remembered: boolean;
  public isUserFinished: boolean = (this.cpf.length == 0 || this.dtNasc.length == 0 || this.cep.length == 0 || this.tel.length == 0 || this.redeem_pass.length == 0) ? false : true;

  public atividadesRecentes = [];
  public dadosExtrato = [];
  public dadosIndicados = [];

  public modalMessage: string;
  public modalTitle: string;
  public modalLoading: boolean;

  public segundosErro = 5;
  public urlJSON = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port;
  public dominioApi = "";

  isUserFinish() {
    this.isUserFinished = (this.cpf.length == 0 || this.dtNasc.length == 0 || this.cep.length == 0 || this.tel.length == 0 || this.redeem_pass.length == 0) ? false : true;
    console.log(this.isUserFinished);
    return (this.cpf.length == 0 || this.dtNasc.length == 0 || this.cep.length == 0 || this.tel.length == 0 || this.redeem_pass.length == 0) ? false : true;
  }

  getConfig() {
    return this.http.get(this.urlJSON + "/assets/json/config.json").toPromise().then(response => {
      this.dominioApi = response.json().dominioApi;
    })
    .catch(response => {
      this.modal('Erro', 'Ocoreu erro em carregar configurações. Tente novamente mais tarde.', false);
    });
  }

  getStatusLogin() {
    let token;
    let member_id;
    token = window.sessionStorage.getItem('tokenMembroVIP');
    member_id = window.sessionStorage.getItem('memberIdMembroVIP');
    if (token == null || member_id == null) {
      token = window.localStorage.getItem('tokenMembroVIP');
      member_id = window.localStorage.getItem('memberIdMembroVIP');
      this.remembered = false;
    } else {
      this.remembered = true;
    }
  }

  async getIndicados() {
    let token;
    let member_id;
    if (this.remembered) {
      token = window.sessionStorage.getItem('tokenMembroVIP');
      member_id = window.sessionStorage.getItem('memberIdMembroVIP');
    } else {
      token = window.localStorage.getItem('tokenMembroVIP');
      member_id = window.localStorage.getItem('memberIdMembroVIP');
    }
    let url = this.dominioApi + 'vipmember/v1/members/' + member_id + '/referrer';
    let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    header.append('Authorization', token);
    return this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(response => {
      let data = response.json();
      data.forEach(element => {
        this.dadosIndicados.push({date: this.dateFormat(element.created_at, true), name: element.name });
      });
    }).catch(response => {
      console.log(response);
      this.modal('Erro', 'Ocorreu um erro em buscar os dados, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this.segundosErro + ' segundos...', false);
      setTimeout(() => this.logout(true), this.segundosErro * 1000);
    })
  }

  async getDados() {
    let token;
    let member_id;
    if (this.remembered) {
      token = window.sessionStorage.getItem('tokenMembroVIP');
      member_id = window.sessionStorage.getItem('memberIdMembroVIP');
    } else {
      token = window.localStorage.getItem('tokenMembroVIP');
      member_id = window.localStorage.getItem('memberIdMembroVIP');
    }
    let url = this.dominioApi + 'vipmember/v1/account/' + member_id + '/entries';
    let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    header.append('Authorization', token);
    return this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(
      response => {
        let dados = response.json();
        this.pontos = dados[0].balance;
        dados = dados[1];
        this.dadosExtrato = [];
        this.atividadesRecentes = [];
        let saldoTemp = 0;
        for (let i = 0; i < dados.length; i++) {
          let pontos = dados[i].type === 'credit' ? dados[i].value : -dados[i].value;
          saldoTemp += pontos;
          this.dadosExtrato.push({ id: dados[i].id, data: new Date(dados[i].created_at), type: dados[i].type, valor: pontos, desc: dados[i].description, saldo: saldoTemp });
        }
        this.dadosExtrato.sort((a, b) => { return b.data - a.data });

        for (let i = 0; i < 4 && i < this.dadosExtrato.length; i++) {
          let valorTemp = this.dadosExtrato[i].valor;
          let valor = (valorTemp === 0) ? valorTemp + " pontos" : (valorTemp > 0) ? ((valorTemp === 1) ? 1 + " ponto" : valorTemp + " pontos") : ((valorTemp === -1) ? -1 + " ponto" : valorTemp + " pontos");
          this.atividadesRecentes.push({ data: this.dateConverter(this.dadosExtrato[i].data), valor: valor, desc: this.dadosExtrato[i].desc });
        }
        this.dadosExtrato.forEach((value, index) => { this.dadosExtrato[index].data = this.dateFormat(value.data, true) });
      }).catch(
      async response => {
        console.log(response);
        this.modal('Erro', 'Ocorreu um erro em buscar os dados, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this.segundosErro + ' segundos...', false);
        await setTimeout(() => this.logout(true), this.segundosErro * 1000);
      });
  }

  async loginValidate() {
    let token;
    let member_id;
    if (this.remembered) {
      token = window.sessionStorage.getItem('tokenMembroVIP');
      member_id = window.sessionStorage.getItem('memberIdMembroVIP');
    } else {
      token = window.localStorage.getItem('tokenMembroVIP');
      member_id = window.localStorage.getItem('memberIdMembroVIP');
    }
    if (token == null || member_id == null) {
      this.modal('Erro', 'É necessário fazer o login para continuar nesta página. Você irá ser redirecionado para o login em ' + this.segundosErro + ' segundos...', false);
      await setTimeout(() => this.logout(true), this.segundosErro * 1000);
    } else {
      let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      header.append('Authorization', token);
      let url = this.dominioApi + 'vipmember/v1/account/' + member_id;
      await this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(async response => { }).catch(async response => {
        if (response.status == 401) {
          this.modal('Erro', 'Sua sessão expirou, entre novamente. Você irá ser redirecionado para o login em ' + this.segundosErro + ' segundos...', false);
          await setTimeout(() => this.logout(true), this.segundosErro * 1000);
        } else {
          this.modal('Erro', 'Ocorreu um erro ao verificar status de login, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this.segundosErro + ' segundos...', false);
          await setTimeout(() => this.logout(true), this.segundosErro * 1000);
        }
      });
    }
  }

  async getUserData() {
    let token;
    let member_id;
    if (this.remembered) {
      token = window.sessionStorage.getItem('tokenMembroVIP');
      member_id = window.sessionStorage.getItem('memberIdMembroVIP');
    } else {
      token = window.localStorage.getItem('tokenMembroVIP');
      member_id = window.localStorage.getItem('memberIdMembroVIP');
    }
    let url = this.dominioApi + 'vipmember/v1/members/' + member_id;
    let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    header.append('Authorization', token);
    return this.http.get(url, new RequestOptions({ headers: header })
    ).toPromise().then(
      response => {
        if (response.status == 200) {
          let data = response.json();
          this.male = (data.gender === "m") ? true : false;
					this.name = data.name;
					this.cpf = data.cpf;
					this.dtNasc = this.dateFormat(data.birth_date, false);
					this.cep = data.cep;
					this.username = data.username;
					this.tel = data.phone;
					this.redeem_pass = data.redeem_pass;
					this.indiqueGanheUrl = 'membro.vip/' + data.username;
        }
      }
      ).catch(async response => {
        this.modal('Erro', 'Ocorreu um erro em carregar os seus dados, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this.segundosErro + ' segundos...', false);
        await setTimeout(() => this.logout(true), this.segundosErro * 1000);
      });
  }

  abrirTwitter() {
    window.open('http://twitter.com/home?status=Ainda%20não%20é%20um%20Membro%20VIP?%0ATenha%20descontos%20exclusivos%20em%20shows,%20trocando%20seus%20pontos!%0ACadastre-se%20no%20link%20abaixo!%0Ahttp%3A%2F%2F' + this.indiqueGanheUrl);
  }

  abrirFacebook() {
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.indiqueGanheUrl + "#signup" + ';src=sdkpreparse', '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
  }

  abrirInstagram() {
    window.open('https://www.instagram.com');
  }

  abrirWhats() {
    window.open('https://web.whatsapp.com/send?text=Ainda%20não%20é%20um%20%2AMembro%20VIP%2A%3F%0ATenha%20descontos%20exclusivos%20em%20shows,%20ganhe%20pontos%20e%20troque%20por%20experiências%20únicas,%20é%20grátis%20e%20rápido.%0ASeja%20um%20%2AMembro%20VIP!%2A%20Cadastre-se%20no%20link%20abaixo!%0Ahttp%3A%2F%2F' + this.indiqueGanheUrl);
  }

  dateFormat(date: string, time: boolean): string {
    let dateTemp = new Date(Date.parse(date));
    if (!time) {
      return (dateTemp.getDate() < 9 ? "0" + (dateTemp.getDate() + 1) : (dateTemp.getDate() + 1)) + "/" + (dateTemp.getMonth() < 9 ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1)) + "/" + dateTemp.getFullYear();
    } else {
      return (dateTemp.getDate() < 9 ? "0" + (dateTemp.getDate() + 1) : (dateTemp.getDate() + 1)) + "/" + (dateTemp.getMonth() < 9 ? "0" + (dateTemp.getMonth() + 1) : (dateTemp.getMonth() + 1)) + "/" + dateTemp.getFullYear() + " " + (dateTemp.getHours() < 10 ? "0" + dateTemp.getHours() : dateTemp.getHours()) + ":" + (dateTemp.getMinutes() < 10 ? "0" + dateTemp.getMinutes() : dateTemp.getMinutes());
    }
  }

  getApiKey() {
    let token;
    if (this.remembered) {
      token = window.sessionStorage.getItem('tokenMembroVIP');
    } else {
      token = window.localStorage.getItem('tokenMembroVIP');
    }
    return token;
  }

  dateConverter(date: Date): string {
    let minutes = 1000 * 60;
    let hour = 1000 * 60 * 60;
    let day = 1000 * 60 * 60 * 24;
    let now = Date.now();
    let difference = now - (date.getTime());
    if (difference < hour) {
      return Math.abs(Math.round(difference / minutes)) + ' Minutos Atrás';
    } else if (difference < day) {
      return Math.abs(Math.round(difference / hour)) + ' Horas Atrás';
    } else {
      return Math.abs(Math.round(difference / day)) + ' Dias Atrás';
    }
  }

  copy() {
    var text = document.querySelector("#indiqueGanheSpam").innerHTML;
    var textArea = document.createElement("textarea");
    textArea.style.position = 'fixed';
    textArea.style.top = '0px';
    textArea.style.left = '0px';
    textArea.style.width = '1px';
    textArea.style.height = '1px';
    textArea.style.padding = '0px';
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();
    try {
      document.execCommand('copy');
      this.modal('Link Indique e Ganhe', 'Seu link foi copiado para a área de transferência!', false);
    } catch (err) {
      this.modal('Erro', 'Erro em copiar o link para a área de transferência.', false);
    }
    document.body.removeChild(textArea);
  }

  modal(title: string, message: string, loading: boolean) {
    document.getElementById('modalClose').click();
    this.modalMessage = message;
    this.modalTitle = title;
    if (loading) {
      this.modalLoading = true;
    } else {
      this.modalLoading = false;
    }
    document.getElementById('modalOpen').click();
  }

  logout(back: boolean): void {
    if (this.remembered) {
      window.sessionStorage.removeItem('tokenMembroVIP');
      window.sessionStorage.removeItem('memberIdMembroVIP');
    } else {
      window.localStorage.removeItem('tokenMembroVIP');
      window.localStorage.removeItem('memberIdMembroVIP');
    }
    if (back) {
      this.router.navigate(['Login']);
    }
  }

  /* ---- */

  constructor(private router: Router, private http: Http) {
    this._dataStream.subscribe((data) => this._onEvent(data));
  }

  notifyDataChanged(event, value) {
    let current = this._data[event];
    if (current !== value) {
      this._data[event] = value;

      this._data.next({
        event: event,
        data: this._data[event]
      });
    }
  }

  subscribe(event: string, callback: Function) {
    let subscribers = this._subscriptions.get(event) || [];
    subscribers.push(callback);
    this._subscriptions.set(event, subscribers);
  }

  _onEvent(data: any) {
    let subscribers = this._subscriptions.get(data['event']) || [];
    subscribers.forEach((callback) => {
      callback.call(null, data['data']);
    });
  }
}
