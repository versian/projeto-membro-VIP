import { Routes, RouterModule } from "@angular/router";
import { LayoutComponent } from "./layout.component";

const LAYOUT_ROUTES: Routes = [
	{
		path: "",
		redirectTo: "Login",
		pathMatch: "full"
	},
	{
		path: "Login",
		loadChildren: "../pages/login/login.module#LoginModule"
	},
	{
		path: "Login/:key",
		loadChildren: "../pages/loginautomatico/loginautomatico.module#LoginAutomaticoModule"
	},
	{
		path: "Cadastro",
		loadChildren: "../pages/cadastro/cadastro.module#CadastroModule"
	},
	{
		path: "Cadastro/:key",
		loadChildren: "../pages/cadastro/cadastro.module#CadastroModule"
	},
	{
		path: "Home",
		component: LayoutComponent,
		children: [
			{
				path: "",
				redirectTo: "Dashboard",
				pathMatch: "full"
			},
			{
				path: "Dashboard",
				loadChildren: "../pages/dashboards/dashboards.module#DashboardsModule"
			},
			{
				path: "MinhaConta",
				children: [
					{
						path: "MeusDados",
						loadChildren: "../pages/meusdados/meusdados.module#MeusDadosModule"
					},
					{
						path: "AlterarSenha",
						loadChildren: "../pages/alterarsenha/alterarsenha.module#AlterarSenhaModule"
					},
					{
						path: "SenhaConsumo",
						loadChildren: "../pages/senhaconsumo/senhaconsumo.module#SenhaConsumoModule"
					}
				]
			},
			{
				path: "FAQ",
				loadChildren: "../pages/faq/faq.module#FaqModule"
			},
			{
				path: "IndiqueGanhe",
				loadChildren: "../pages/indiqueganhe/indiqueganhe.module#IndiqueGanheModule"
			},
			{
				path: "Programacao",
				loadChildren: "../pages/programacao/programacao.module#ProgramacaoModule"
			},
			{
				path: "Extrato",
				loadChildren: "../pages/extrato/extrato.module#ExtratoModule"
			},
			{
				path: "MeusIndicados",
				loadChildren: "../pages/meusindicados/meusindicados.module#MeusIndicadosModule"
			},
			{
				path: "MeusIngressos",
				loadChildren: "../pages/meusingressos/meusingressos.module#MeusIngressosModule"
			},
			{
				path: "Show",
				loadChildren: "../pages/show/show.module#ShowModule"
			},
			{
				path: "Show/:hash",
				loadChildren: "../pages/show/show.module#ShowModule"
			},
			{
				path: "IngressoVirtual",
				loadChildren: "../pages/ingressovirtual/ingressovirtual.module#IngressoVirtualModule"
			},
			{
				path: "IngressoVirtual/:hash",
				loadChildren: "../pages/ingressovirtual/ingressovirtual.module#IngressoVirtualModule"
			}
			
		]
	},

	// 404 Page Not Found
	{ path: "**", redirectTo: "" }
];

export const LayoutRoutes = RouterModule.forChild(LAYOUT_ROUTES);
