import {
	Component,
	ViewEncapsulation,
	OnInit,
	trigger,
	state,
	style,
	transition,
	animate,
	ElementRef,
	HostListener
} from "@angular/core";
import { GlobalState } from "../../app.state";
import { ConfigService } from "../../shared/services/config/config.service";

@Component({
	selector: "app-sidebar",
	templateUrl: "./left-sidebar.component.html",
	styleUrls: ["./left-sidebar.component.scss"]
})
export class LeftSidebarComponent implements OnInit {
	public scrollbarOptions = {
		axis: "y",
		theme: "minimal",
		scrollInertia: 0,
		mouseWheel: { preventDefault: true }
	};

	constructor(public config: ConfigService, private _elementRef: ElementRef, public _state: GlobalState) {
		this._state.subscribe("app.isCollapsed", isCollapsed => {
			this.config.appLayout.isApp_SidebarLeftCollapsed = isCollapsed;
		});
	}
	ngOnInit() { }

	clickMenu() {
		if (document.body.clientWidth <= 500) {
			setTimeout(() => {
				this.config.appLayout.isApp_SidebarLeftCollapsed = !this.config.appLayout.isApp_SidebarLeftCollapsed;
			}, 500);
			setTimeout(() => {
				document.getElementsByTagName("app-root").item(0).classList.remove("app_sidebar-menu-collapsed");
				document.getElementsByTagName("app-root").item(0).classList.remove("app_sidebar-left-open");
				document.getElementById("backdrop").firstElementChild.classList.remove("appBackdrop");
			}, 500);
			/*
			setTimeout(() => {
				document.getElementsByTagName("app-root").item(0).classList.remove("app_sidebar-menu-collapsed");
				document.getElementsByTagName("app-root").item(0).classList.remove("app_sidebar-left-open");	
			}, 500);
			*/
		}
	}

	toggleMenuSideabar() {
		if (document.body.clientWidth <= 500) {
			let left = document.getElementsByTagName("app-root").item(0).classList.contains("app_sidebar-menu-collapsed") && document.getElementsByTagName("app-root").item(0).classList.contains("app_sidebar-left-open") && document.getElementById("backdrop").firstElementChild.classList.contains("appBackdrop");
			if(!left) {
				document.getElementsByTagName("app-root").item(0).classList.add("app_sidebar-menu-collapsed");
				document.getElementsByTagName("app-root").item(0).classList.add("app_sidebar-left-open");
				document.getElementById("backdrop").firstElementChild.classList.add("appBackdrop");
			} else {
				document.getElementsByTagName("app-root").item(0).classList.remove("app_sidebar-menu-collapsed");
				document.getElementsByTagName("app-root").item(0).classList.remove("app_sidebar-left-open");
				document.getElementById("backdrop").firstElementChild.classList.remove("appBackdrop");
			}
		}
		console.log(this.config.appLayout.isApp_SidebarLeftCollapsed);
		this.config.appLayout.isApp_SidebarLeftCollapsed = !this.config.appLayout.isApp_SidebarLeftCollapsed;
		this._state.notifyDataChanged("app.isCollapsed", this.config.appLayout.isApp_SidebarLeftCollapsed);
	}
	@HostListener("window:resize")
	public onWindowResize(): void { }
}
