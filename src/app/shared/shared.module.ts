// Angular
// https://angular.io/
import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

// Angular Material
// https://material.angular.io/
import {
	MdAutocompleteModule,
	MdButtonModule,
	MdButtonToggleModule,
	MdCardModule,
	MdCheckboxModule,
	MdChipsModule,
	MdCoreModule,
	MdDatepickerModule,
	MdDialogModule,
	MdExpansionModule,
	MdGridListModule,
	MdIconModule,
	MdInputModule,
	MdListModule,
	MdMenuModule,
	MdNativeDateModule,
	MdProgressBarModule,
	MdProgressSpinnerModule,
	MdRadioModule,
	MdRippleModule,
	MdSelectModule,
	MdSidenavModule,
	MdSliderModule,
	MdSlideToggleModule,
	MdSnackBarModule,
	MdTabsModule,
	MdToolbarModule,
	MdTooltipModule,
	StyleModule
} from "@angular/material";
import { NguUtilityModule } from "ngu-utility/ngu-utility.module";
import { MalihuScrollbarModule } from "ngx-malihu-scrollbar";
// ngx-bootstrap4
// http://valor-software.com/ngx-bootstrap/index-bs4.html#/
import { TabsModule } from "ngx-bootstrap/tabs";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { AlertModule } from "ngx-bootstrap/alert";
import { ModalModule } from "ngx-bootstrap/modal";
import { PopoverModule } from "ngx-bootstrap/popover";

// angular2-moment
// https://github.com/urish/angular2-moment
import { MomentModule } from "angular2-moment";

// ng-gauge
// https://github.com/subarroca/ng-gauge
import { GaugeModule } from "ng-gauge";

//Wizard
import { FormWizardModule } from "angular2-wizard";
//Form Validation
// https://github.com/yuyang041060120/ng2-validation
import { CustomFormsModule } from "ng2-validation";

//ng2-datepicker
//https://github.com/jkuri/ng2-datepicker
import { DatePickerModule } from "ng2-datepicker";

//ng-daterangepicker
//https://github.com/jkuri/ng-daterangepicker
import { NgDateRangePickerModule } from "ng-daterangepicker";

//Drag and drop
import { DndModule } from "ng2-dnd";
//Chartist
import { ChartistModule } from "ng-chartist";
// UI Shared Components
import { FooterComponent } from "../layout/footer/footer.component";
import { AppBackdropComponent } from "./components/app_backdrop/app_backdrop.component";
import { ImageCardComponent } from "./components/cards/image-card/image-card.component";
import { TabsOverCardComponent } from "./components/cards/tabs-over-card/tabs-over-card.component";
import { SocialCardComponent } from "./components/cards/social-card/social-card.component";
import { ImageOverCardComponent } from "./components/cards/image-over-card/image-over-card.component";
import { TextMaskModule } from 'angular2-text-mask';
import {
	SmdFabSpeedDialActionsComponent,
	SmdFabSpeedDialComponent,
	SmdFabSpeedDialTriggerComponent
} from "./components/fab/index";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MomentModule,
		MdAutocompleteModule,
		MdButtonModule,
		MdButtonToggleModule,
		MdCardModule,
		MdCheckboxModule,
		MdChipsModule,
		MdCoreModule,
		MdDatepickerModule,
		MdDialogModule,
		MdExpansionModule,
		MdGridListModule,
		MdIconModule,
		MdInputModule,
		MdListModule,
		MdMenuModule,
		MdNativeDateModule,
		MdProgressBarModule,
		MdProgressSpinnerModule,
		MdRadioModule,
		MdRippleModule,
		MdSelectModule,
		MdSidenavModule,
		MdSliderModule,
		MdSlideToggleModule,
		MdSnackBarModule,
		MdTabsModule,
		MdToolbarModule,
		MdTooltipModule,
		StyleModule,
		NguUtilityModule,
		GaugeModule,
		FormWizardModule,
		CustomFormsModule,
		DatePickerModule,
		ChartistModule,
		NgDateRangePickerModule,
		BsDropdownModule.forRoot(),
		AlertModule.forRoot(),
		TabsModule.forRoot(),
		MalihuScrollbarModule.forRoot(),
		ModalModule.forRoot(),
		PopoverModule.forRoot(),
		DndModule.forRoot()
	],
	declarations: [
		AppBackdropComponent,
		FooterComponent,
		ImageCardComponent,
		TabsOverCardComponent,
		SocialCardComponent,
		SmdFabSpeedDialActionsComponent,
		SmdFabSpeedDialComponent,
		SmdFabSpeedDialTriggerComponent,
		ImageOverCardComponent
	],
	exports: [
		CommonModule,
		ImageOverCardComponent,
		TextMaskModule,
		FormsModule,
		MomentModule,
		MdAutocompleteModule,
		MdButtonModule,
		MdButtonToggleModule,
		MdCardModule,
		MdCheckboxModule,
		MdChipsModule,
		MdCoreModule,
		MdDatepickerModule,
		MdDialogModule,
		MdExpansionModule,
		MdGridListModule,
		MdIconModule,
		MdInputModule,
		MdListModule,
		MdMenuModule,
		MdNativeDateModule,
		MdProgressBarModule,
		MdProgressSpinnerModule,
		MdRadioModule,
		MdRippleModule,
		MdSelectModule,
		MdSidenavModule,
		MdSliderModule,
		MdSlideToggleModule,
		MdSnackBarModule,
		MdTabsModule,
		MdToolbarModule,
		MdTooltipModule,
		StyleModule,
		NguUtilityModule,
		AppBackdropComponent,
		FooterComponent,
		ImageCardComponent,
		TabsOverCardComponent,
		SocialCardComponent,
		ReactiveFormsModule,
		TabsModule,
		BsDropdownModule,
		AlertModule,
		MalihuScrollbarModule,
		ModalModule,
		PopoverModule,
		FormWizardModule,
		GaugeModule,
		CustomFormsModule,
		DatePickerModule,
		ChartistModule,
		DndModule,
		NgDateRangePickerModule,
		SmdFabSpeedDialActionsComponent,
		SmdFabSpeedDialComponent,
		SmdFabSpeedDialTriggerComponent
	]
})
export class SharedModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: SharedModule
		};
	}
}
