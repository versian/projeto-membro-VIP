import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IndiqueGanheComponent } from './indiqueganhe.component';
import { SharedModule } from '../../shared/shared.module';


const INDIQUEGANHE_ROUTE = [
    { path: '', component: IndiqueGanheComponent },
];

@NgModule({
	  declarations: [IndiqueGanheComponent],
    imports: [
			CommonModule,
			SharedModule,
			RouterModule.forChild(INDIQUEGANHE_ROUTE)
    ]
  
})
export class IndiqueGanheModule { }
