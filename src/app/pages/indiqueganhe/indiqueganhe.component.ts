import { Component, OnInit } from '@angular/core';
import { GlobalState } from '../../app.state';
import { ConfigService } from '../../shared/services/config/config.service';
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { Http, Headers, RequestOptions } from "@angular/http";
import { Router } from "@angular/router";

@Component({
  selector: '.content_inner_wrapper',
  templateUrl: './indiqueganhe.component.html',
  styleUrls: ['./indiqueganhe.component.scss']
})
export class IndiqueGanheComponent implements OnInit {
  multi;
  displayMode;

  public duvidasIndiqueGanhe;

  constructor(
    public config: ConfigService,
    public _state: GlobalState,
    private _spinner: SpinnerService,
    private http: Http,
    private router: Router
  ) { }

  async ngOnInit() {
    this._spinner.show();
    await this._state.getConfig().then(async response => {
      this._state.getStatusLogin();
      await this._state.loginValidate();
      await this._state.getUserData().then(response => {
        if (!this._state.isUserFinish()) {
          this.router.navigate(['Home/MinhaConta/MeusDados']);
        } else {
          let endereco = this._state.urlJSON;
          this.http.get(endereco + '/assets/json/indique_ganhe.json', new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8' }) })).toPromise().then(
            response => {
              let data = response.json();
              for (let i = 0; i < data.length; i++) {
                data[i].pergunta = data[i].pergunta.replace('{indiqueGanheUrl}', '"' + this._state.indiqueGanheUrl + '"');
                data[i].resposta = data[i].resposta.replace('{indiqueGanheUrl}', '"' + this._state.indiqueGanheUrl + '"');
              }
              this.duvidasIndiqueGanhe = data;
            }).catch(
            async response => {
              this._state.modal('Erro', 'Ocorreu um erro ao carregar a página, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
              await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
            });
        }
      });
    }).catch(response => { });
    this._spinner.hide();
  }

}
