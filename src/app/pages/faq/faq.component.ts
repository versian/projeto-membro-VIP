import {
	Component,
	OnInit
} from "@angular/core";
import { GlobalState } from "../../app.state";
import { Router } from "@angular/router";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { Http, Headers } from "@angular/http";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./faq.component.html",
	styleUrls: ["./faq.component.scss"]
})
export class FaqComponent implements OnInit {
	multi;
	displayMode;

	public faqPerguntas;

	constructor(private _spinner: SpinnerService, public _state: GlobalState, private router: Router, private http: Http) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this._state.loginValidate();
			await this._state.getUserData().then(async response => {
				if (!this._state.isUserFinish()) {
					this.router.navigate(['Home/MinhaConta/MeusDados']);
				} else {
					let endereco = this._state.urlJSON;
					await this.http.get(endereco + '/assets/json/faq.json').toPromise().then(
						response => {
							this.faqPerguntas = response.json();
						}).catch(
						async response => {
							this._state.modal('Erro', 'Ocorreu um erro ao carregar a página, tente novamente mais tarde.<br />Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
							await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
						});
				}
			});
		}).catch(response => { });
		this._spinner.hide();
	}

}
