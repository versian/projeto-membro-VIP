import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FaqComponent } from "./faq.component";
import { SharedModule } from "../../shared/shared.module";
import { Ng2SearchPipeModule } from "ng2-search-filter";

const FAQ_ROUTE = [{ path: "", component: FaqComponent }];

@NgModule({
	declarations: [FaqComponent],
	imports: [
		CommonModule,
		SharedModule,
		Ng2SearchPipeModule,
		RouterModule.forChild(FAQ_ROUTE)
	]
})
export class FaqModule {}
