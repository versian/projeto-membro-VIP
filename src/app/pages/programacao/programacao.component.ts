import {
	Component,
	OnInit
} from "@angular/core";
import { GlobalState } from "../../app.state";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { Router } from "@angular/router";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./programacao.component.html",
	styleUrls: ["./programacao.component.scss"]
})
export class ProgramacaoComponent implements OnInit {

	shows = [];

	constructor(
		public _state: GlobalState,
		private http: Http,
		private _spinner: SpinnerService,
		private router: Router
	) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			await this._state.loginValidate();
			await this._state.getStatusLogin();
			await this._state.getUserData().then(response => {
				if (!this._state.isUserFinish()) {
					this.router.navigate(['Home/MinhaConta/MeusDados']);
				} else {
					this._state.getProgramacao().then(response => {this._spinner.hide()}).catch(response => { console.log(response);this._spinner.hide();});
				}
			}).catch(response => { console.log(response);this._spinner.hide();});
		}).catch(response => { console.log(response);this._spinner.hide();});
		
	}

/*
	getProgramacao() {
		let token;
		let member_id;
		if (this._state.remembered) {
			token = window.sessionStorage.getItem('tokenMembroVIP');
			member_id = window.sessionStorage.getItem('memberIdMembroVIP');
		} else {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}
		let url = this._state.dominioApi + 'vipmember/v1/shows/';
		let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		header.append('Authorization', token);
		return this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(response => {
			let data = response.json();
			this.shows = data;
		}).catch(response => {
				console.log(response);
				this._state.modal('Erro', 'Ocoreu erro em carregar os dados dos shows. Tente novamente mais tarde.', false);
		});
	}
	*/

	openPage(show) {
		let token;
		let member_id;

		token = window.sessionStorage.getItem('tokenMembroVIP');
		member_id = window.sessionStorage.getItem('memberIdMembroVIP');

		if (token == null || member_id == null) {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}

		let url = "https://membrovip.estancianativa.com.br/event.php?id=" + show.hash + "&key=" + token;
		this._state.modal("Redirecionamento","Você está sendo redirecionado ao site EnterTicket, responsável pela venda on-line de ingressos.",false);
		setTimeout(() => {window.open(url, "_self");}, this._state.segundosErro * 1000);
	}
}