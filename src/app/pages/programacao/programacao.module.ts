import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ProgramacaoComponent } from "./programacao.component";
import { SharedModule } from "../../shared/shared.module";

const PROGRAMACAO_ROUTE = [{ path: "", component: ProgramacaoComponent }];

@NgModule({
	declarations: [ProgramacaoComponent],
	imports: [
		CommonModule,
		SharedModule,
		RouterModule.forChild(PROGRAMACAO_ROUTE)
	],
	providers: []
})
export class ProgramacaoModule {}
