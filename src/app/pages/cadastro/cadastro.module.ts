import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CadastroComponent } from "./cadastro.component";
import { SharedModule } from "../../shared/shared.module";

const CADASTRO_ROUTE = [{ path: "", component: CadastroComponent }];

@NgModule({
	declarations: [CadastroComponent],
	imports: [CommonModule, SharedModule, RouterModule.forChild(CADASTRO_ROUTE)]
})
export class CadastroModule {}
