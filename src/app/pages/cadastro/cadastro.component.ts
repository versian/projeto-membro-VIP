import {
	Component,
	Input,
	OnInit
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./cadastro.component.html",
	styleUrls: ["./cadastro.component.scss"]
})
export class CadastroComponent implements OnInit {

	loading: boolean = false;
	senha = "";
	repetirSenha = "";
	nome = "";
	email = "";

	modalType: number = 1;

	sub;
	id;

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState,
		private route: ActivatedRoute
	) { }

	async ngOnInit() {
		this._spinner.show();
		this.sub = this.route.params.subscribe(async params => {
			this.id = params['key'] || "";
			await this._state.getConfig().then(async response => {
			}).catch(response => { });
		});
		this._spinner.hide();
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	voltar() {
		this.router.navigate(["Login"]);
	}

	async cadastrar() {
		if (this.conferirCampos()) {
			this._state.modal('Fazendo o Cadastro...', 'Aguarde...', true);
			let url = this._state.dominioApi + 'vipmember/v1/members/initial';
			let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
			let data = 'name=' + this.nome + "&email=" + this.email + "&password=" + this.senha + "&referrer_id=" + this.id;
			await this.http.post(url, data, new RequestOptions({ headers: header })).toPromise().then(
				response => {
					this.modalType = 2;
					this._state.modal('Cadastro', 'Conta criada com sucesso! Clique no botão abaixo e faça seu login.', false);
				}
			).catch(
				async response => {
					if (response.status === 422) {
						this.modalType = 2;
						this._state.modal('Erro', 'Este email "' + this.email + '" já está cadastrado como Membro VIP. Clique no botão abaixo e acesse sua conta', false);
					} else {
						this._state.modal('Erro', 'Ocorreu um erro realizar o cadastro, tente novamente mais tarde.', false);
					}
				});
		}
	}

	conferirCampos() {
		this.modalType = 1;
		if (!(this.email.indexOf("@") > -1)) {
			this._state.modal("Erro", "Endereço de e-mail inválido.", false);
			return false;
		}
		else if (this.senha.length < 5) {
			this._state.modal('Erro', 'A sua senha deve conter no mínimo 5 caracteres', false);
			return false;
		} else if(this.repetirSenha.length == 0) {
			this._state.modal('Erro', 'Confirme sua senha', false);
			return false;
		} else if (this.senha !== this.repetirSenha) {
			this._state.modal('Erro', 'Por favor, repita sua senha corretamente', false);
			return false;
		}
		return true;
	}


}