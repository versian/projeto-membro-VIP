import {
	Component,
	OnInit
} from "@angular/core";
import { GlobalState } from "../../app.state";
import { ConfigService } from "../../shared/services/config/config.service";
import { Http, RequestOptions, Headers } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { Router } from "@angular/router";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./meusdados.component.html",
	styleUrls: ["./meusdados.component.scss"]
})
export class MeusDadosComponent implements OnInit {
	multi;
	displayMode;

	public nome: string = "";
	public cpf: string = "";
	public sexo: boolean;
	public dataNasc: string = "";
	public cep: string = "";
	public telefone: string = "";
	public redeem_pass: string = "";
	public username: string = "";

	public cpfMask = [/[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
	public dataNascMask = [/[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
	public cepMask = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /\d/, /\d/, /\d/];
	public telefoneMask = ['(', /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /(\d+)/, '-', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
	public redeem_passMask = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
	public guide = false;

	public cpfDesabilitado: boolean = false;
	public userDesabilitado: boolean = false;
	public redeemDesabilitado: boolean = false;

	constructor(
		public _state: GlobalState,
		private _spinner: SpinnerService,
		private http: Http,
		private router: Router
	) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this.loginValidate();
			await this.getUserData().then(response => {
				this.nome = this._state.name;
				this.cpf = this._state.cpf;
				this.sexo = this._state.male;
				this.dataNasc = this._state.dtNasc;
				this.cep = this._state.cep;
				this.telefone = this._state.tel;
				this.redeem_pass = this._state.redeem_pass;
				this.username = this._state.username;
				if (!this._state.isUserFinish()) {
					document.getElementById('modalInicialOpen').click();
				}
			});
		}).catch(response => { });
		this._spinner.hide();
	}

	async getUserData() {
		let token;
		let member_id;
		if (this._state.remembered) {
			token = window.sessionStorage.getItem('tokenMembroVIP');
			member_id = window.sessionStorage.getItem('memberIdMembroVIP');
		} else {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}
		let url = this._state.dominioApi + '/vipmember/v1/members/' + member_id;
		let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		header.append('Authorization', token);
		return this.http.get(url, new RequestOptions({ headers: header })
		).toPromise().then(
			response => {
				if (response.status == 200) {
					let data = response.json();
					console.log(data);
					this._state.male = (data.gender === "m") ? true : false;
					this._state.name = (data.name) || "";
					this._state.cpf = (data.cpf) || "";
					this._state.dtNasc = this._state.dateFormat(((data.birth_date) || ""), false);
					this._state.cep = (data.cep) || "";
					this._state.username = (data.username) || "";
					this._state.tel = (data.phone) || "";
					this._state.redeem_pass = (data.redeem_pass) || "";
					this._state.indiqueGanheUrl = 'membro.vip/' + this._state.username;

					if (this._state.cpf.length > 0) {
						this.cpfDesabilitado = true;
					} else {
						this.cpfDesabilitado = false;
					}

					if (this._state.username.length > 0) {
						this.userDesabilitado = true;
					} else {
						this.userDesabilitado = false;
					}

					if (this._state.redeem_pass.length > 0) {
						this.redeemDesabilitado = true;
					} else {
						this.redeemDesabilitado = false;
					}
				}
			}).catch(async response => {
				this._state.modal('Erro', 'Ocorreu um erro em carregar os seus dados, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
				await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
			});
	}

	cancelarDados() {
		this.router.navigate(['Home/Dashboard']);
	}

	async loginValidate() {
		let token;
		let member_id;
		if (this._state.remembered) {
			token = window.sessionStorage.getItem('tokenMembroVIP');
			member_id = window.sessionStorage.getItem('memberIdMembroVIP');
		} else {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}
		if (token == null || member_id == null) {
			this._state.modal('Erro', 'Sua sessão expirou, entre novamente. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
			await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
			return;
		}
		let url = this._state.dominioApi + 'vipmember/v1/account/' + member_id;
		let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		header.append('Authorization', token);
		await this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(response => { if (response.status == 200) { } }).catch(async response => {
			if (response.status == 401) {
				this._state.modal('Erro', 'Sua sessão expirou, entre novamente. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
				await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
			} else {
				this._state.modal('Erro', 'Ocorreu um erro ao verificar status de login, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
				await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
			}
		});
	}

	async enviarDados() {
		if ((this.cpf.split('.').join('').replace('-','').length == 0 || this.dataNasc.replace("_", "").length == 0 || this.cep.replace("_", "").length == 0 || this.telefone.replace("_", "").length == 0 || this.redeem_pass.replace("_", "").length < 4)) {
			this._state.modal('Erro', 'Para completar o cadastro, é necessário preencher todos os campos!', false);
		} else if(!(this.validarCpf(this.cpf.split('.').join('').replace('-','')))) {
			this._state.modal('Erro', 'Para se completar o cadastro, é necessário preencher um CPF válido.', false);
		} else if(!this.validarData(this.dataNasc)) {
			this._state.modal('Erro', 'Para se completar o cadastro, é necessário preencher uma DATA válida.', false);
		} else if(this.username.indexOf(' ') > -1) {
			this._state.modal('Erro', 'Para se completar o cadastro, o NOME DE USUÁRIO não deve conter espaços.', false);
		} else {
			await this._state.loginValidate();
			this._state.modal('Alterando...', 'Aguarde...', true);
			let token;
			let member_id;
			if (this._state.remembered) {
				token = window.sessionStorage.getItem('tokenMembroVIP');
				member_id = window.sessionStorage.getItem('memberIdMembroVIP');
			} else {
				token = window.localStorage.getItem('tokenMembroVIP');
				member_id = window.localStorage.getItem('memberIdMembroVIP');
			}

			let dataConvertedArr = this.dataNasc.split('/');
			let dataConverted = dataConvertedArr[2] + "-" + (+dataConvertedArr[1]) + "-" + (+dataConvertedArr[0]);
			let cpf = this.cpf.split('.').join('').replace('-','');
			let cep = this.cep.replace("-", "");
			let phone = this.telefone.replace("-", "").replace("(", "").replace(")", "").replace(" ", "");
			let data = "username=" + (this.userDesabilitado ? this._state.username : this.username.trim()) + "&name=" + this.nome.trim() + "&cpf=" + (this.cpfDesabilitado ? this._state.cpf.split('.').join('') : cpf) + "&gender=" + ((this.sexo) ? 'm' : 'f') + "&birth_date=" + dataConverted + "&cep=" + cep + "&redeem_pass=" + (this.redeemDesabilitado ? this._state.redeem_pass : this.redeem_pass) + "&phone=" + phone;
			let url = this._state.dominioApi + 'vipmember/v1/members/' + member_id + '?' + data;
			let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
			header.append('Authorization', token);
			await this.http.put(url, data, new RequestOptions({ headers: header })).toPromise().then(
				response => {
					if (response.status == 200) {
						document.getElementById("modalClose").click();
						this._state.modal('Alteração de Dados', 'Dados Alterados com Sucesso!', false);
					}
				}
			).catch(
				async response => {
					if (response.status == 401) {
						this._state.modal('Erro', 'Sua sessão expirou, entre novamente. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
						await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
					} else if (response.status == 422) {
						this._state.modal('Erro', response.json().message, false);
					} else {
						this._state.modal('Erro', 'Ocorreu um erro ao alterar os dados, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
						await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
					}
				});
		}
	}

	selecionarSexo(sexo: boolean) {
		this.sexo = sexo;
	}

	validarCpf(cpf: string): boolean {
		var Soma;
		var Resto;
		Soma = 0;
		if (cpf == "00000000000")
			return false;
		for (let i = 1; i <= 9; i++)
			Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(cpf.substring(9, 10)))
			return false;
		Soma = 0;
		for (let i = 1; i <= 10; i++)
			Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;
		if ((Resto == 10) || (Resto == 11))
			Resto = 0;
		if (Resto != parseInt(cpf.substring(10, 11)))
			return false;
			
		return true;
	}

	validarData(data: string): boolean {
		var dia = data.substring(0,2)
		var mes = data.substring(3,5)
		var ano = data.substring(6,10)
	 
		var novaData = new Date(parseInt(ano),(parseInt(mes)-1),parseInt(dia));
	 
		var mesmoDia = parseInt(dia,10) == novaData.getDate();
		var mesmoMes = parseInt(mes,10) == (novaData.getMonth()+1);
		var mesmoAno = parseInt(ano) == novaData.getFullYear();
	 
		if (!((mesmoDia) && (mesmoMes) && (mesmoAno)))
		{  
			return false;
		}  
		return true;
	}

}