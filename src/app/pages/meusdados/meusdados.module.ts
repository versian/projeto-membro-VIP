import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MeusDadosComponent } from './meusdados.component';
import { SharedModule } from '../../shared/shared.module';

const MEUS_DADOS_ROUTE = [
    { path: '', component: MeusDadosComponent },
];

@NgModule({
	  declarations: [MeusDadosComponent],
    imports: [
			CommonModule,
			SharedModule,
			RouterModule.forChild(MEUS_DADOS_ROUTE)
    ]
  
})
export class MeusDadosModule { }
