import {
	Component,
	Input,
	OnInit
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"]
})

export class LoginComponent implements OnInit {

	email: string = "";
	password: string = "";
	remember: boolean = false;
	emailChecked: boolean = false;

	toggleRegister: boolean = false;
	loading: boolean = false;

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState
	) { }

	async ngOnInit() {
		this._spinner.show();
		this._state.clickMobileTrigger = false;
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this.loginValidate();
			setTimeout(() => {
				try{
					document.getElementById('email').focus();
				}catch(err){
					
				}}, 100);

		}).catch(response => {});
		
		this._spinner.hide();
	}

	abrirCadastro() {
		this.router.navigate(["Cadastro"]);
	}

	async checkEmail() {
		this.loading = true;
		if (this.email.length === 0) {
			this._state.modal('Erro', 'Por favor, digite um e-mail.', false);
		} else {
			let url = this._state.dominioApi + 'vipmember/v1/login/checkEmail';
			let data = "email=" + this.email;
			let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
			await this.http.post(url, data, new RequestOptions({ headers: header })).toPromise().then(
				response => {
					if (response.status == 200) {
						if (response.json().status === "incomplete") {
							this._state.modal('Cadastro Incompleto', response.json().message, false);
						} else {
							this.emailChecked = true;
							setTimeout(() => {document.getElementById('password').focus()}, 100);
						}
					}
				}
			).catch(
				response => {
					if (response.status == 404) {
						this._state.modal('Erro', response.json().message, false);
					}
				}
				);
		}
		this.loading = false;
	}

	async login() {
		this.loading = true;
		let url = this._state.dominioApi + 'vipmember/v1/login/authenticate';
		if (this.password.length === 0) {
			this._state.modal('Erro', 'Por favor, digite a senha.', false);
		} else {
			let data = "email=" + this.email + "&password=" + this.password;
			let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
			await this.http.post(url, data, new RequestOptions({ headers: header })).toPromise().then(
				response => {
					if (response.status == 200) {
						if (response.json().status === "user incomplete") {
							this._state.modal('Cadastro Incompleto', response.json().message, false);
						} else {
							if (this.remember) {
								sessionStorage.setItem('tokenMembroVIP', response.json().api_key); // Token
								sessionStorage.setItem('memberIdMembroVIP', response.json().member_id);
								sessionStorage.setItem('pwdMembroVIP', this.password);
								this._state.password = this.password;
								this.router.navigate(['Home/Dashboard']);
							} else {
								localStorage.setItem('tokenMembroVIP', response.json().api_key); // Token
								localStorage.setItem('memberIdMembroVIP', response.json().member_id);
								localStorage.setItem('pwdMembroVIP', this.password);
								this._state.password = this.password;
								this.router.navigate(['Home/Dashboard']);
							}
						}
					}
				}
			).catch(
				response => {
					if (response.status != 401) {
						this._state.modal('Erro', 'Ocorreu um erro ao realizar o login, tente novamente mais tarde.', false);
					} else {
						this._state.modal('Erro', 'Usuário ou senha incorreta.', false);
					}
				}
			);
		}
		setTimeout(() => {this.loading = false}, 1000);
	}

	async loginValidate() {
		let token;
		let member_id;
		if (this._state.remembered) {
			token = window.sessionStorage.getItem('tokenMembroVIP');
			member_id = window.sessionStorage.getItem('memberIdMembroVIP');
		} else {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}
		if (token != null || member_id != null) {
			let url = this._state.dominioApi + 'vipmember/v1/account/' + member_id;
			let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
			header.append('Authorization', token);
			await this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(
				response => {
					if (response.status == 200) {
						this.router.navigate(['Home/Dashboard']);
					}
				}
			).catch(response => {
				if (response.status == 401) {
					this._state.modal('Erro', 'Sua sessão expirou, entre novamente.', false);
					this._state.logout(false);
				} else {
					this._state.modal('Erro', 'Ocorreu um erro ao verificar status de login, tente novamente mais tarde.', false);
				}
			});
		}
	}

}