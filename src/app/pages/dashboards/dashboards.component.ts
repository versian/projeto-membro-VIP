import {
	Component,
	OnInit
} from "@angular/core";
import { GlobalState } from "../../app.state";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { Router } from "@angular/router";
import { Http } from "@angular/http";
import { Inject } from "@angular/core";
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./dashboards.component.html",
	styleUrls: ["./dashboards.component.scss"],
})
export class DashboardsComponent implements OnInit {
	multi;
	displayMode;

	public duvidasDash;

	constructor(
		public _state: GlobalState,
		private _spinner: SpinnerService,
		private router: Router,
		private http: Http,
		public dialog: MdDialog
	) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this._state.loginValidate();
			await this._state.getUserData().then(async response => {
				if (!this._state.isUserFinish()) {
					this.router.navigate(['Home/MinhaConta/MeusDados']);
				} else {
					await this._state.getDados().then(async response => {
						let endereco = this._state.urlJSON;
						await this.http.get(endereco + '/assets/json/duvidas.json').toPromise().then(
							response => {
								this.duvidasDash = response.json();
								this._spinner.hide();
							}).catch(
							async response => {
								console.log(response);
								this._state.modal('Erro', 'Ocorreu um erro ao carregar a página, tente novamente mais tarde. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
								await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
								this._spinner.hide();
							});
					});
				}
			});
		}).catch(response => { console.log(response);this._spinner.hide(); });
	}
}