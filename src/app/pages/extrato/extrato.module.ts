import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ExtratoComponent } from "./extrato.component";
import { SharedModule } from "../../shared/shared.module";

const EXTRATO_ROUTE = [{ path: "", component: ExtratoComponent }];

@NgModule({
	declarations: [ExtratoComponent],
	imports: [CommonModule, SharedModule, RouterModule.forChild(EXTRATO_ROUTE)]
})
export class ExtratoModule {}
