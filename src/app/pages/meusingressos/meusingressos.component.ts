import {
	Component,
	Input,
	OnInit
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./meusingressos.component.html",
	styleUrls: ["./meusingressos.component.scss"]
})
export class MeusIngressosComponent implements OnInit {

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState
	) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this._state.loginValidate();
			await this._state.getUserData().then(async response => {
				if (!this._state.isUserFinish()) {
					this.router.navigate(['Home/MinhaConta/MeusDados']);
					return;
				}
				await this._state.getIngressos().then(response => {
					this._spinner.hide();
				}).catch(response => {this._spinner.hide();});
			});
		}).catch(response => { console.log(response);this._spinner.hide(); });
	}
}