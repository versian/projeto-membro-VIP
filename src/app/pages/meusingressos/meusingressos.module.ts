import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MeusIngressosComponent } from "./meusingressos.component";
import { SharedModule } from "../../shared/shared.module";


const MEUS_INGRESSOS_ROUTE = [{ path: "", component: MeusIngressosComponent }];

@NgModule({
	declarations: [MeusIngressosComponent],
	imports: [
		CommonModule, 
		SharedModule, 
		RouterModule.forChild(MEUS_INGRESSOS_ROUTE)
	]
})

export class MeusIngressosModule {}