import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MeusIndicadosComponent } from "./meusindicados.component";
import { SharedModule } from "../../shared/shared.module";

const INDICADOS_ROUTE = [{ path: "", component: MeusIndicadosComponent }];

@NgModule({
	declarations: [MeusIndicadosComponent],
	imports: [CommonModule, SharedModule, RouterModule.forChild(INDICADOS_ROUTE)]
})
export class MeusIndicadosModule {}
