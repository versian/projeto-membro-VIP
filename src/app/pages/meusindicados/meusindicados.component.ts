import {
	Component,
	Input,
	OnInit
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./meusindicados.component.html",
	styleUrls: ["./meusindicados.component.scss"]
})
export class MeusIndicadosComponent implements OnInit {

	pagAtual = 0;
	pags = [];
	qtdObjPags = 5;
	qtdPaginas;
	semIndicados = false;

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState
	) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this._state.loginValidate();
			await this._state.getUserData().then(async response => {
				if (!this._state.isUserFinish()) {
					this.router.navigate(['Home/MinhaConta/MeusDados']);
				} else {
					await this._state.getIndicados().then(response => {
						this.qtdPaginas = (this._state.dadosIndicados.length / this.qtdObjPags);
						this.prepararIndicados(this._state.dadosIndicados);
						if(this._state.dadosIndicados.length === 0) {
							this.semIndicados = true;
						}
					});
				}
			});
		}).catch(response => { console.log(response) });
		this._spinner.hide();
	}

	prepararIndicados(indicados) {
		let qtdPaginas = (indicados.length / this.qtdObjPags);
		if (qtdPaginas <= 1) {
			this.pags[0] = indicados;
		} else {
			for (let contador = 0; contador < qtdPaginas; contador++) {
				this.pags[contador] = [];
				for (let i = 0; i < this.qtdObjPags; i++) {
					let valor = indicados[i + (this.qtdObjPags * contador)];
					if (!(valor === undefined)) {
						this.pags[contador].push(indicados[i + (this.qtdObjPags * contador)]);
					} else {
						break;
					}
				}
			}
		}
	}

	next() {
		if (!((this.pags.length - 1) === this.pagAtual)) {
			this.pagAtual++;
		}
	}

	prev() {
		if (!(this.pagAtual === 0)) {
			this.pagAtual--;
		}
	}

}