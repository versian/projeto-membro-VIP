import {
	Component,
	Input,
	OnInit
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./senhaconsumo.component.html",
	styleUrls: ["./senhaconsumo.component.scss"]
})
export class SenhaConsumoComponent implements OnInit {

	public password = "";
	public redeem_pass = "";
	public redeem_pass_confirm = "";

	public guide = false;
	public redeem_passMask = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState
	) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this._state.loginValidate();
			await this._state.getUserData().then(response => {
				if (!this._state.isUserFinish()) {
					this.router.navigate(['Home/MinhaConta/MeusDados']);
				}
			});
		}).catch(response => { });
		this._spinner.hide();
	}

	senhasOk() {
		if (this.redeem_pass == this.redeem_pass_confirm && this.redeem_pass.length === 4 && this.password.length > 0 && this.password === this._state.password) {
			return true;
		}
		return false;
	}

	trocarSenha() {
		if (this.senhasOk()) {
			let token;
			let member_id;
			if (this._state.remembered) {
				token = window.sessionStorage.getItem('tokenMembroVIP');
				member_id = window.sessionStorage.getItem('memberIdMembroVIP');
			} else {
				token = window.localStorage.getItem('tokenMembroVIP');
				member_id = window.localStorage.getItem('memberIdMembroVIP');
			}
			let url = this._state.dominioApi + 'vipmember/v1/members/' + member_id; // Rota
			let data = 'redeem_pass=' + this.redeem_pass;
			let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
			header.append('Authorization', token);
			return this.http.patch(url, data, new RequestOptions({ headers: header })).toPromise().then(response => {
				this._state.modal("Alteração de Senha", "Senha alterada com sucesso!", false);
			}).catch(response => {
				this._state.modal("Erro", "Erro em trocar as senhas. Verifique se as senhas estão digitadas corretamente, e tente novamente", false);
			});
		} else {
			if (this.password != this._state.password) {
				this._state.modal("Erro", "Sua senha de acesso está incorreta", false);
			} else {
				this._state.modal("Erro", "Os campos Nova Senha e Confirmar Senha devem ser idênticos", false);
			}
		}
	}

}