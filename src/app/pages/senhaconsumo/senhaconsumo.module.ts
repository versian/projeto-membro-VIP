import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { SenhaConsumoComponent } from "./senhaconsumo.component";
import { SharedModule } from "../../shared/shared.module";

const SENHA_CONSUMO_ROUTE = [{ path: "", component: SenhaConsumoComponent }];

@NgModule({
	declarations: [SenhaConsumoComponent],
	imports: [
		CommonModule, 
		SharedModule, 
		RouterModule.forChild(SENHA_CONSUMO_ROUTE)
	]
})
export class SenhaConsumoModule {}
