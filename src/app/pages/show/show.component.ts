import {
	Component,
	Input,
	OnInit,
	OnDestroy
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./show.component.html",
	styleUrls: ["./show.component.scss"]
})
export class ShowComponent implements OnInit, OnDestroy {

	private sub;
	public hash;

	public pontosComprar;
	public showComprar;
	public tipoIngressoSelecionado = "floor";

	public show = { data: "", nome: "", valor_front: 0, valor_lane: 0, imagem: "" };
	compraDisponivel(): boolean { return (this.tipoIngressoSelecionado === 'floor') ? this.show.valor_lane <= this._state.pontos : this.show.valor_front <= this._state.pontos };
	public senhaConsumo = "";

	public redeem_passMask = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
	public guide = false;


	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState,
		private route: ActivatedRoute
	) { }

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	async ngOnInit() {
		this._spinner.show();
		this.sub = this.route.params.subscribe(async params => {
			this._state.clickMobileTrigger = false;
			this.hash = params['hash'] || "";
			await this._state.getConfig().then(async response => {
				this._state.getStatusLogin();
				await this._state.loginValidate();
				await this._state.getDados().then(async response => {
					await this._state.getUserData().then(async response => {
						if (!this._state.isUserFinish()) {
							this.router.navigate(['Home/MinhaConta/MeusDados']);
							return;
						}
						await this.getInformacoesShow().then(response => {
							this._spinner.hide();
						});
					});
				}).catch(response => { this._spinner.hide(); });
			}).catch(response => { this._spinner.hide(); });
		});
	}

	getInformacoesShow() {
		let token;
		let member_id;
		if (this._state.remembered) {
			token = window.sessionStorage.getItem('tokenMembroVIP');
			member_id = window.sessionStorage.getItem('memberIdMembroVIP');
		} else {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}
		let url = this._state.dominioApi + 'vipmember/v1/shows/' + this.hash; // Rota
		let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		header.append('Authorization', token);
		return this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(response => {
			let data = response.json(); // Data
			if (Object.getOwnPropertyNames(data).length > 0) {
				this.show = { data: this.formatar(data[0].event_date), nome: data[0].name, imagem: data[0].image_path, valor_front: parseInt(data[0].value_front), valor_lane: parseInt(data[0].value_lane) }
			}
		}).catch(response => {
			this._state.modal("Erro", "Erro em buscar os dados do show. Tente novamente mais tarde", false);
		});
	}

	comprar() {
		if (this.compraDisponivel()) {
			if (this.senhaConsumo === this._state.redeem_pass) {
				let token;
				let member_id;
				if (this._state.remembered) {
					token = window.sessionStorage.getItem('tokenMembroVIP');
					member_id = window.sessionStorage.getItem('memberIdMembroVIP');
				} else {
					token = window.localStorage.getItem('tokenMembroVIP');
					member_id = window.localStorage.getItem('memberIdMembroVIP');
				}
				let url = this._state.dominioApi + 'vipmemberpayment/v1/redeem/' + this._state.cpf + '/ticket';
				let data = 'redeem_pass=' + this._state.redeem_pass + '&event_hash=' + this.hash + '&value=' + ((this.tipoIngressoSelecionado == 'front') ? this.show.valor_front : this.show.valor_lane) + '&place=' + ((this.tipoIngressoSelecionado == 'front') ? 'front' : 'floor');
				let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
				header.append('Authorization', token);
				return this.http.post(url, data, new RequestOptions({ headers: header })).toPromise().then(response => {
					this._state.modal("Compra com Pontos", "Compra efetuada com sucesso! Verifique a página 'Meus Ingressos' para visualizar seu ingresso virtual.", false);
					setTimeout(() => {this._state.clickMobileTrigger = false; this.router.navigate(['Home/MeusIngressos'])}, 2500);
				}).catch(response => {
					console.log(response);
					if(response.status == 422) {
						this._state.modal("Erro", response.json().message, false);
					} else if(response.status == 404) {
						this._state.modal("Erro", "Show não encontrado", false);
					} else { 
						this._state.modal("Erro", "Ocorreu um erro, tente novamente mais tarde", false);
						setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
					}
				});
			} else {
				this._state.modal("Erro", "Senha de consumo inválida", false)
			}
		}
	}

	formatar(dataTexto: string) {
		var extenso;
		let data = new Date(dataTexto);
		var day = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"][data.getDay()];
		var date = data.getDate();
		var month = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"][data.getMonth()];
		var year = data.getFullYear();

		return (`${day}, ${date} de ${month} de ${year}`);
	}

}