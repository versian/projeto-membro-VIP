import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ShowComponent } from "./show.component";
import { SharedModule } from "../../shared/shared.module";

const SHOW_ROUTE = [{ path: "", component: ShowComponent }];

@NgModule({
	declarations: [ShowComponent],
	imports: [
		CommonModule, 
		SharedModule, 
		RouterModule.forChild(SHOW_ROUTE)
	]
})
export class ShowModule {}
