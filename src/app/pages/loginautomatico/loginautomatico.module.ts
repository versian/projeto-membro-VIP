import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LoginAutomaticoComponent } from "./loginautomatico.component";
import { SharedModule } from "../../shared/shared.module";

const LOGIN_AUTOMATICO_ROUTE = [{ path: "", component: LoginAutomaticoComponent }];

@NgModule({
	declarations: [LoginAutomaticoComponent],
	imports: [
		CommonModule, 
		SharedModule,
		RouterModule.forChild(LOGIN_AUTOMATICO_ROUTE)
	]
})
export class LoginAutomaticoModule {}
