import {
	Component,
	Input,
	OnInit,
	OnDestroy
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./loginautomatico.component.html",
	styleUrls: ["./loginautomatico.component.scss"]
})
export class LoginAutomaticoComponent implements OnInit, OnDestroy {

	private sub;

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState,
		private route: ActivatedRoute
	) { }

	async ngOnInit() {
		this._spinner.show();
		let key;
		this.sub = this.route.params.subscribe(params => {
			key = params['key'];

			this._state.getConfig().then(response => {
				let url = this._state.dominioApi + 'vipmember/v1/login/' + key;
				let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
				this.http.post(url, new RequestOptions({ headers: header })
				).toPromise().then(
					response => {
						if (response.json().status === "user incomplete") {
							this._state.modal('Cadastro Incompleto', response.json().message, false);
							this.router.navigate(['Login']);
						} else {
							localStorage.setItem('tokenMembroVIP', response.json().api_key);
							localStorage.setItem('memberIdMembroVIP', response.json().member_id);
							this.router.navigate(['Home/Dashboard']);
						}
					}).catch(async response => {
						console.log(response);
						await setTimeout(() => this._state.logout(true));
					});
			});
		});
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

}