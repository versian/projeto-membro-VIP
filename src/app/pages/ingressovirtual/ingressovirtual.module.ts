import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { IngressoVirtualComponent } from "./ingressovirtual.component";
import { SharedModule } from "../../shared/shared.module";

const INGRESSO_VIRTUAL_ROUTE = [{ path: "", component: IngressoVirtualComponent }];

@NgModule({
	declarations: [IngressoVirtualComponent],
	imports: [
		CommonModule, 
		SharedModule, 
		RouterModule.forChild(INGRESSO_VIRTUAL_ROUTE)
	]
})

export class IngressoVirtualModule {}