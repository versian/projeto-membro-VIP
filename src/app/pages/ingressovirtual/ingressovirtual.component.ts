import {
	Component,
	Input,
	OnInit,
	OnDestroy
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./ingressovirtual.component.html",
	styleUrls: ["./ingressovirtual.component.css"]
})
export class IngressoVirtualComponent implements OnInit, OnDestroy {

	private sub;
	public hash;

	public nome;
	public cpf;
	public id;
	public show;
	public data;
	public qtd;
	public tipo;
	public valor;

	public encontrado = true;

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState,
		private route: ActivatedRoute
	) { }

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	async ngOnInit() {
		this._spinner.show();
		this.sub = this.route.params.subscribe(async params => {
			this.hash = params['hash'] || "";
			this._state.clickMobileTrigger = false;
			await this._state.getConfig().then(async response => {
				this._state.getStatusLogin();
				await this._state.loginValidate();
				await this._state.getDados().then(async response => {
					await this._state.getUserData().then(async response => {
						if (!this._state.isUserFinish()) {
							this.router.navigate(['Home/MinhaConta/MeusDados']);
							return;
						}
						await this.getIngressoVirtual().then(response => {
							this._spinner.hide();
						}).catch(response => {
							this._spinner.hide();
						});
						
					});
				}).catch(response => { this._spinner.hide();});
			}).catch(response => { this._spinner.hide();});
		});
	}

	async getIngressoVirtual() {
		let token;
		let member_id;
		if (this._state.remembered) {
			token = window.sessionStorage.getItem('tokenMembroVIP');
			member_id = window.sessionStorage.getItem('memberIdMembroVIP');
		} else {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}
		let url = this._state.dominioApi + '/vipmember/v1/tickets/' + member_id + '/' + this.hash; // Rota
		let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		header.append('Authorization', token);
		return this.http.get(url, new RequestOptions({ headers: header })).toPromise().then(
			response => {
				let data = response.json();
				this.cpf = this._state.cpf;
				this.nome = this._state.name;
				this.show = data.event_name;
				this.id = data.ticket_code;
				this.data = this._state.dateFormat(data.event_date, false);
				this.tipo = data.ticket_place === 'floor' ? "Pista" : "Front-Stage";
				this.valor = parseInt(data.value);
				//this.qtd = data.qtd;
			}
		).catch(
			async response => {
				if(response.status == 404) {
					this.encontrado = false;
				} else {
					console.log(response);
					this._state.modal("Erro", "Ocorreu um erro em buscar dados do ingresso, tente novamente mais tarde.", false);
				}
			});
	}

	formatarCpf(cpf : string)  {
		if(cpf !== undefined)
			return '' + cpf[0] + cpf[1] + cpf[2] + '.' + cpf[3] + cpf[4] + cpf[5] + '.' + cpf[6] + cpf[7] + cpf[8] + "-" + cpf[9] + cpf[10];
	}

}