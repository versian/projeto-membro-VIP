import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AlterarSenhaComponent } from "./alterarsenha.component";
import { SharedModule } from "../../shared/shared.module";

const ALTERAR_SENHA_ROUTE = [{ path: "", component: AlterarSenhaComponent }];

@NgModule({
	declarations: [AlterarSenhaComponent],
	imports: [
		CommonModule, 
		SharedModule, 
		RouterModule.forChild(ALTERAR_SENHA_ROUTE)
	]
})
export class AlterarSenhaModule {}
