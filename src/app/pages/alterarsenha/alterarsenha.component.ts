import {
	Component,
	Input,
	OnInit
} from "@angular/core";

import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpinnerService } from "../../shared/services/spinner/spinner.service";
import { GlobalState } from "../../app.state";

@Component({
	selector: ".content_inner_wrapper",
	templateUrl: "./alterarsenha.component.html",
	styleUrls: ["./alterarsenha.component.scss"]
})
export class AlterarSenhaComponent implements OnInit {

	novaSenha = "";
	confirmarSenha = "";
	passwordNow = "";

	constructor(
		private router: Router,
		private _spinner: SpinnerService,
		private http: Http,
		public _state: GlobalState
	) { }

	async ngOnInit() {
		this._spinner.show();
		await this._state.getConfig().then(async response => {
			this._state.getStatusLogin();
			await this._state.loginValidate();
			await this._state.getUserData().then(response => {
				if (!this._state.isUserFinish()) {
					this.router.navigate(['Home/MinhaConta/MeusDados']);
				}
			});
		}).catch(response => { });
		this._spinner.hide();
	}

	confirmarSenhas() {
		if (this.novaSenha !== this.confirmarSenha || this.novaSenha.length === 0 || this.passwordNow.length === 0 || this.passwordNow != this._state.password) {
			return false;
		}
		return true;
	}

	async trocarSenha() {
		if(this.passwordNow != this._state.password) {
			this._state.modal('Erro', 'A senha atual de acesso está incorreta.', false);
			return;
		}
		if (!this.confirmarSenhas()) {
			this._state.modal('Erro', 'Os campos "Nova Senha" e "Confirmar Senha" devem ser idênticos', false);
			return;
		}

		this._state.modal('Trocando a Senha...', 'Aguarde...', true);
		let token;
		let member_id;
		if (this._state.remembered) {
			token = window.sessionStorage.getItem('tokenMembroVIP');
			member_id = window.sessionStorage.getItem('memberIdMembroVIP');
		} else {
			token = window.localStorage.getItem('tokenMembroVIP');
			member_id = window.localStorage.getItem('memberIdMembroVIP');
		}
		let url = this._state.dominioApi + 'vipmember/v1/members/' + member_id;
		let header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		header.append('Authorization', token);
		let data = "password=" + this.novaSenha;
		await this.http.patch(url, data, new RequestOptions({ headers: header })).toPromise().then(
			response => {
				if (response.status == 200) {
					document.getElementById("modalClose").click();
					setTimeout(() => { this._state.modal('Troca de Senha', 'Troca da senha realizada com sucesso!', false) }, 200);
					if(this._state.remembered) {
						window.sessionStorage.setItem('pwdMembroVIP', this.novaSenha);
					} else {
						window.localStorage.setItem('pwdMembroVIP', this.novaSenha);
					}
					this._state.password = this.novaSenha;
				}
			}
		).catch(
			async response => {
				if (response.status == 401) {
					this._state.modal('Erro', 'Sua sessão expirou, entre novamente. Você irá ser redirecionado para o login em ' + this._state.segundosErro + ' segundos...', false);
					await setTimeout(() => this._state.logout(true), this._state.segundosErro * 1000);
				} else {
					this._state.modal('Erro', 'Ocorreu um erro ao trocar a senha, tente novamente mais tarde.', false);
				}
			});
	}

}